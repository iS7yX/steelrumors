from django.db import models
from django.db.models import Count
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.core.urlresolvers import reverse

class LinkVoteCountManager(models.Manager):
	def get_queryset(self):
		return super(LinkVoteCountManager, self).get_queryset().annotate(
			votes=Count('vote')).order_by('-votes')


class Link(models.Model):
	title = models.CharField('Headline', max_length=100)
	submitter = models.ForeignKey(User)
	submitted_on = models.DateTimeField(auto_now_add=True)
	rank_score = models.FloatField(default=0.0)
	url = models.URLField('URL', max_length=250, blank=True)
	description = models.TextField(blank=True)

	with_votes = LinkVoteCountManager()
	objects = models.Manager()

	def __unicode__(self):
		return self.title

	def get_absolute_url(self):
		return reverse('link_detail', kwargs={'pk': str(self.id),})

class Vote(models.Model):
	voter = models.ForeignKey(User)
	link = models.ForeignKey(Link)

	def __unicode__(self):
		return "{0} voted for {1}".format(self.voter.username,
			self.link.title)

class UserProfile(models.Model):
	user = models.OneToOneField(User, unique=True)
	#Extra attributes here
	bio = models.TextField(null=True)

	def __unicode__(self):
		return "{0}'s profile".format(self.user)

def create_profile(sender, instance, created, **kwargs):
	''' A signal handler to ensure whenever a User is created, A UserProfile
	is created as well.
	'''
	if created:
		profile, created = UserProfile.objects.get_or_create(user=instance)

post_save.connect(create_profile, sender=User)
